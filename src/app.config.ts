/*
 * @Author: xx
 * @Date: 2024-02-05 23:01:47
 * @LastEditors: Zhanghan
 * @LastEditTime: 2025-02-13 20:29:26
 * @Description:
 */
export default defineAppConfig({
  pages: [
    "pages/home/index",
    "pages/mine/index",
    "pages/menu/index",
    "pages/shopcar/index",
  ],
  window: {
    backgroundTextStyle: "light",
    navigationBarBackgroundColor: "#fff",
    navigationBarTitleText: "商城",
    navigationBarTextStyle: "black",
  },
  tabBar: {
    position: "bottom",
    selectedColor: "#44CBB4",
    borderStyle: "black",
    // custom: true,
    list: [
      {
        pagePath: "pages/home/index",
        text: "首页",
        iconPath: "assests/my_tabBar/home.png",
        selectedIconPath: "assests/my_tabBar/home_active.png",
      },
      {
        pagePath: "pages/menu/index",
        text: "分类",
        iconPath: "assests/my_tabBar/menu.png",
        selectedIconPath: "assests/my_tabBar/menu_active.png",
      },
      {
        pagePath: "pages/shopcar/index",
        text: "购物车",
        iconPath: "assests/my_tabBar/shop.png",
        selectedIconPath: "assests/my_tabBar/shop_active.png",
      },
      {
        pagePath: "pages/mine/index",
        text: "个人中心s",
        iconPath: "assests/my_tabBar/mine.png",
        selectedIconPath: "assests/my_tabBar/mine_active.png",
      },
    ],
  },
});
