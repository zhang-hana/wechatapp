import  baseHttp  from '@/http/request';

const API_HOME={
    SWIPPER:'banner/list',
    SHOPLIST:'shop/goods/list'
};

/**
 * @Description: 获取轮播图 
 * @Author: Zhanghan
 * @Date: 2024-02-08 20:09:52
 */
export async function getSwipper(param:any) {
    return baseHttp({
        url:API_HOME.SWIPPER,
        data:param,
        needToken:true
    })
}

/**
 * @Description: 获取商品列表 
 * @Author: Zhanghan
 * @Date: 2024-02-08 20:11:38
 */
export async function getshopList(param:any){
    return baseHttp({
        url:API_HOME.SHOPLIST,
        data:param,
        needToken:true
    })
}