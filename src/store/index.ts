/*
 * @Description: 创建store仓库
 * @Author: Zhanghan
 * @Date: 2024-02-06 22:31:26
 * @LastEditTime: 2024-02-07 20:55:39
 * @LastEditors: Zhanghan
 */
import type { App } from 'vue';
import { createPinia } from 'pinia';

const store = createPinia();

export function setupStore(app: App<Element>) {
  app.use(store);
}

export { store };