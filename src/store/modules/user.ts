import { defineStore } from 'pinia';

export const useUserStore = defineStore({
    id:'app-user',
    state: () => ({
        // user info
        userInfo: '这是用户名',
        // Last fetch time
        lastUpdateTime: 0,
      }),
      getters: {
        getUserInfo(){
            return this.userInfo
        }
      },
      actions: {
        updapteUserInfo(){
            this.userInfo = '更改用户名'
        }
      }
})