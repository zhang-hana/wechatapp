import { defineStore } from "pinia";

export const usePageStore = defineStore({
  id: "app-page",
  state: () => ({
    // Last fetch time
    selected: 0,
  }),
  getters: {
    // 当前选中的tabbar
    getTabbar() {
      return this.selected;
    },
  },
  actions: {
    changeTabbar(index) {
      this.selected = index;
    },
  },
});
