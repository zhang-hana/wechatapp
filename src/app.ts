import { createApp } from "vue";
import { Button, Toast, Swiper, Row, Col , SwiperItem ,BackTop ,Icon } from "@nutui/nutui-taro";
import "./app.scss";
import { setupStore } from "./store";

const App = createApp({
  onShow(options) {},
  // 入口组件不需要实现 render 方法，即使实现了也会被 taro 所覆盖
});

App.use(Button)
  .use(Toast)
  .use(Swiper)
  .use(Row)
  .use(Col)
  .use(SwiperItem)
  .use(Icon)
  .use(BackTop)
  //仓库开启
setupStore(App);

export default App;
